// Author: Nikola Hardi 3/6/2019
#ifndef ROOT_TJClientFile
#define ROOT_TJClientFile

#include <cstdio>
#include <unistd.h>

#include "TObject.h"
#include "TString.h"

using std::string;

struct TJClientFile : public TObject {
  TJClientFile(const char *filepath = NULL);
  bool loadFile(const char *filepath = NULL);
  TString getDefaultPath();
  TString getTmpdir();

  TString sUsercert;
  TString sUserkey;
  TString fHost;
  TString fHome;
  TString fUser;
  TString fPw;
  TString tmpdir;
  TString defaultJClientPath;
  int fPort;
  int fWSPort;
  bool isValid;

  ClassDef(TJClientFile, 0)
};

#endif
