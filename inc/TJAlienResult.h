// @(#)root/alien:$Id$
// Author: Fons Rademakers   3/1/2002

/*************************************************************************
 * Copyright (C) 1995-2002, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_TJAlienResult
#define ROOT_TJAlienResult

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TAliEnResult                                                     //
//                                                                      //
// Class defining interface to a Alien result set.                      //
// Objects of this class are created by TGrid methods.                  //
//                                                                      //
// Related classes are TAlien.                                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#ifndef ROOT_TGridResult
#include "TGridResult.h"
#endif

#ifndef ROOT_TMap
#include<TMap.h>
#endif
#include "TObjString.h"
#define CMDEND '\0'

class TJAlienResult : public TGridResult {

private:
   mutable TString fFilePath;  // file path
   TMap *fJAliEnMetaData;

public:
   TJAlienResult();
   virtual ~TJAlienResult();

   virtual void DumpResult();
   virtual const char       *GetFileName(UInt_t i) const;             // returns the file name of list item i
   virtual const char       *GetFileNamePath(UInt_t i) const;         // returns the full path + file name of list item i
   virtual const TEntryList *GetEntryList(UInt_t i) const;            // returns an entry list, if it is defined
   virtual const char       *GetPath(UInt_t i) const;                 // returns the file path of list item i
   virtual const char       *GetKey(UInt_t i, const char *key) const; // returns the key value of list item i
   virtual Bool_t            SetKey(UInt_t i, const char *key, const char *value); // set the key value of list item i
   virtual TList            *GetFileInfoList() const;                 // returns a new allocated List of TFileInfo Objects
   using                     TCollection::Print;
   virtual void              Print(Option_t *option = "") const;

   void Add(TObject *) override; // special Add function (to perform some checks)
   
   TObjString *GetMetaData(TObjString const* key) const;
   void SetMetaData(TObjString *key, TObjString *value);

   ClassDef(TJAlienResult,0)  // Alien query result set
};

#endif
