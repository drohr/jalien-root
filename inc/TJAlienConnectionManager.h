// Author: Volodymyr Yurchenko 27/06/2019

#ifndef ROOT_TJAlienConnectionManager
#define ROOT_TJAlienConnectionManager

#if !defined(__CINT__) && !defined(__MAKECINT__) && !defined(__ROOTCLING__) && !defined(__CLING__)
#include <json-c/json.h>
#else
struct json_object;
#endif

#include <sys/stat.h>
#if !defined(__CINT__) && !defined(__MAKECINT__) && !defined(__ROOTCLING__) && !defined(__CLING__)
#include <libwebsockets.h>
#include "lws_config.h"
#else
struct lws_context;
struct lws_context_creation_info;
#endif

#include "TJAlienResult.h"
#include "TJAlienCredentials.h"
#include "TJAlienDNSResolver.h"
#include "TJClientFile.h"
#include "TError.h"
#include "TGrid.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <fstream>
#include <memory>

#define DEFAULT_JCENTRAL_SERVER "alice-jcentral.cern.ch"
#define UNUSED(x) (void)(x)

class TJAlienConnectionManager {
private:
    const int default_WSport = 8097;

    const std::string default_server = DEFAULT_JCENTRAL_SERVER;
    std::string fWSHost;     // websocket host
    int         fWSPort;     // websocket port
    TString     sUsercert;   // location of user certificate
    TString     sUserkey;    // location of user private key

    // Libwebsockets
    static int destroy_flag;                // Flags to know connection status
    static int connection_flag;
    static int writeable_flag;
    static int receive_flag;

    constexpr static int default_ping_interval = 10;  // ping interval
    constexpr static int default_timeout = 20;   // no __network__ __activity__ this interval, let's close/timeout
    const static int lws_ping_interval; // std::getenv("JALIEN_PING_INTERVAL") ? std::stoi(std::getenv("JALIEN_PING_INTERVAL")) : default_ping_interval;
    const static int lws_timeout; // std::getenv("JALIEN_PING_TIMEOUT") ? std::stoi(std::getenv("JALIEN_PING_TIMEOUT")) : default_timeout;

    #if LWS_LIBRARY_VERSION_NUMBER > 4000000
    static const lws_retry_bo_t retry; // { .secs_since_valid_ping = lws_ping_interval, .secs_since_valid_hangup = lws_timeout };
    #endif

    struct lws_context *context;            // Context contains all information about connection
    /* struct lws_context_creation_info creation_info;	// Info to create logical connection */
    struct lws *wsi;                        // WebSocket Instance - real connection object, created basing on context

    #if !defined(__CINT__) && !defined(__MAKECINT__) && !defined(__ROOTCLING__) && !defined(__CLING__)
    static int ws_service_callback(         // Callback to handle connection
        struct lws *wsi,
        enum lws_callback_reasons reason, void *user,
        void *in, size_t len);
    static int websocket_write_back(struct lws *wsi_in, const char *str, int str_size_in);
    #endif

    static size_t WriteCallback(void *contents, size_t size, size_t nmemb);
    void clearFlags();
    TJAlienCredentials creds;
    static std::string readBuffer;

public:
    TJAlienConnectionManager() {} // default constructor
    ~TJAlienConnectionManager();
    int CreateConnection();
    void ConnectJBox(TJAlienCredentialsObject const& c);
    void ConnectJCentral(TJAlienCredentialsObject const& c, string host = DEFAULT_JCENTRAL_SERVER);
    void MakeWebsocketConnection(TJAlienCredentialsObject const& creds, string host, int WSPort);
    void ForceRestart();
    std::unique_ptr<TJAlienResult> RunJsonCommand(TString const& command, TList const* options);
    std::unique_ptr<TJAlienResult> RunJsonCommand(TString const& command, TList const* options, std::map<std::string, TString>* metadata, std::string *readBuffer);

    // Parse the result from Json structure
    std::unique_ptr<TJAlienResult> GetCommandResult(json_object *json_response, bool expand_find = false);
 private:
    // Format command to Json structure
    json_object* CreateJsonCommand(TString const& command, TList const* options);
 public:
    virtual Bool_t IsConnected() const;

    ClassDef(TJAlienConnectionManager, 0)
};
#endif
