#include "TJClientFile.h"
#include <fstream>
#include "TError.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TString.h"

using std::ifstream;

TJClientFile::TJClientFile(const char *filepath) {
    tmpdir = getTmpdir();

    if(filepath != NULL) {
        this->isValid = loadFile(filepath);
    } else {
        this->isValid = loadFile(getDefaultPath());
    }
}

TString TJClientFile::getDefaultPath() {
    return tmpdir + TString("/jclient_token_") + TString::Itoa(getuid(),10);
}

TString TJClientFile::getTmpdir() {
    TString tmpdir;

    if (getenv("TMPDIR") != NULL)
        tmpdir = getenv("TMPDIR");
    else if (getenv("TMP") != NULL)
        tmpdir = getenv("TMP");
    else if (getenv("TEMP") != NULL)
        tmpdir = getenv("TEMP");
    else
        tmpdir = P_tmpdir;

    return tmpdir;
}


bool TJClientFile::loadFile(const char *filepath) {
    if(filepath == NULL) {
        return false;
    }

    ifstream jclientFile(filepath);
    string fileLine;

    bool result = true;

    if (jclientFile.is_open())
    {
        while (getline(jclientFile, fileLine))
        {
            if (gDebug > 1) Info("TJAlien", "Token file line: %s", fileLine.c_str());

            TString sLine = fileLine;
            TObjArray *arr = sLine.Tokenize("= ");

            if (arr->GetEntries() == 2)
            {
                TObjString *a = (TObjString*) arr->At(0);
                TObjString *b = (TObjString*) arr->At(1);

                TString sKey = a->GetString();
                TString sValue = b->GetString();

                if (gDebug > 1) Info("TJAlien", "\"%s\" = \"%s\"", sKey.Data(), sValue.Data());

                if (sKey.EqualTo("JALIEN_HOST"))
                {
                    fHost = sValue;

                    if (fHost.Length() == 0)
                    {
                        Error("TJAlien", "JAliEn connection host field empty");
                        result = false;
                        delete arr;
                        break;
                    }
                }

                if (sKey.EqualTo("JALIEN_PORT"))
                {
                    fPort = sValue.Atoi();
                }

                if (sKey.EqualTo("JALIEN_WSPORT"))
                {
                    fWSPort = sValue.Atoi();

                    if (fWSPort == 0)
                    {
                        Error("TJAlien", "JAliEn connection port field empty or misspelled");
                        result = false;
                        delete arr;
                        break;
                    }
                }

                if (sKey.EqualTo("JALIEN_HOME"))
                {
                    fHome = sValue;
                }

                if (sKey.EqualTo("JALIEN_USER"))
                {
                    fUser = sValue;
                }

                if (sKey.EqualTo("JALIEN_PASSWORD"))
                {
                    fPw = sValue;
                }
            }
            else
            {
                if (gDebug > 1) Error("TJAlien", "jclient file does not have "
                                    "the correct structure (%d)", arr->GetEntries());
                result = false;
            }

            delete arr;
        }

        jclientFile.close();
    }
    else
    {
        if (gDebug > 1) Error("TJAlien", "Error while opening jclient file");
        result = false;
    }

    return result;
}
